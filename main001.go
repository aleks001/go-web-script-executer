package main

import (
	"time"
	"bytes"
	"log"
	"fmt"
	"net/http"
	"github.com/davecgh/go-spew/spew"
	"os/exec"
	"os"
	"regexp"
)

type logWriter struct {
}

func (writer logWriter) Write(bytes []byte) (int, error) {
	return fmt.Printf("%s [ci-cd-logger] %+v",time.Now().Format(time.RFC3339), string(bytes))
}

// https://play.golang.org/p/8qWeAekujk

type LoggedMux struct {
	*http.ServeMux
	log *log.Logger
}

func NewLoggedMux(logger *log.Logger) *LoggedMux {
	var mux = &LoggedMux{log: logger}
	mux.ServeMux = http.NewServeMux()
	if mux.log == nil {
		mux.log = log.New(os.Stderr, "", log.LstdFlags)
	}
	return mux
}

func (mux *LoggedMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	mux.ServeMux.ServeHTTP(w, r)
	mux.log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)

	if os.Getenv("DEBUG") != "" {
		mux.log.Printf("spew.Sdump ", spew.Sdump(r))
	}
}

func handler (w http.ResponseWriter, r *http.Request){

	var mybuffer bytes.Buffer
	reqlogger := log.New(&mybuffer,"[in_handler] ", log.Lshortfile)
	reqlogger.SetOutput(new(logWriter))

	var cmd_to_exec  string = os.Getenv("SCRIPT_TO_EXEC")
	reqlogger.Printf("Execute %s", cmd_to_exec)

	cmd := exec.Command(cmd_to_exec)

	var out bytes.Buffer
	cmd.Stdout = &out
	var stder bytes.Buffer
	cmd.Stderr = &stder

	err := cmd.Run()

	if err != nil {
	  reqlogger.Printf("ERR %s",spew.Sdump(err))
	  reqlogger.Printf("STDERR %s",spew.Sdump(stder))
	}else {
		// success don't need to be logged
		if os.Getenv("DEBUG") != "" {
			reqlogger.Printf("STDOUT %s", spew.Sdump(out))
		}
	}
}

func main() {
	var mybuffer bytes.Buffer
	logger := log.New(&mybuffer,"", log.Lshortfile)
	logger.SetOutput(new(logWriter))

	if os.Getenv("CI_CD_LISTEN") == "" {
		logger.Print("No 'CI_CD_LISTEN' in env found")
		os.Exit(1)
	}else {
		var matched bool
		matched, err := regexp.MatchString("^127.0.0.1", os.Getenv("CI_CD_LISTEN"))
		if ! matched {
			logger.Print("REGEX not matched ",matched, err)
			logger.Print("CI_CD_LISTEN :",os.Getenv("CI_CD_LISTEN"),":")
			logger.Print("REGEX :^127.0.0.1:")
		}
	}

	if os.Getenv("SCRIPT_TO_EXEC") == "" {
		logger.Print("No 'SCRIPT_TO_EXEC' in env found")
		os.Exit(2)
	}else {
		var checked os.FileInfo

		checked,err := os.Stat(os.Getenv("SCRIPT_TO_EXEC"))
		if os.IsNotExist(err) {
			logger.Print("File ",os.Getenv("SCRIPT_TO_EXEC"),"not found ", checked)
			os.Exit(3)
		}
	}

	var mux = NewLoggedMux(logger)
	var s = &http.Server{Addr: os.Getenv("CI_CD_LISTEN"), Handler: mux}

	logger.Print("Listening on ",os.Getenv("CI_CD_LISTEN"), " start time is", time.Now().Format(time.RFC3339))

	// TODO: add env for url path
	s.Handler.(*LoggedMux).HandleFunc("/ci-cd", handler)
	s.ListenAndServe()
}
