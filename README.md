# Small Helper for webhooks

## General

**ATENTION: DANGEROUS PROGRAM!**

This small go tool listen on `CI_CD_LISTEN` port and
executes the script defined in `SCRIPT_TO_EXEC`.

It's my first go programm ;-)

## release

The bin is in `.idea/bin/`

## Output of a simple bitbucket webhook call.

```
bitbuck@observer:~$ env CI-CD-LISTEN=127.0.0.1:8055 SCRIPT-TO-EXEC=/bin/true ./go-web-script-executer
```

```
2016-12-04T14:43:58+01:00 [testLogger] main001.go:108: The start time is 2016-12-04T14:43:58+01:00

2016-12-04T14:45:37+01:00 [testLogger] [in_handler] main001.go:52: Execute /bin/true

2016-12-04T14:45:37+01:00 [testLogger] [in_handler] main001.go:68: STDOUT (bytes.Buffer)

2016-12-04T14:45:37+01:00 [testLogger] main001.go:41: 127.0.0.1:39089 GET /ci-cd

2016-12-04T14:45:37+01:00 [testLogger] main001.go:42: spew.Sdump %!(EXTRA string=(*http.Request)(0xc4200c60f0)({
 Method: (string) (len=3) "GET",
 URL: (*url.URL)(0xc4200e8000)(/ci-cd),
 Proto: (string) (len=8) "HTTP/1.1",
 ProtoMajor: (int) 1,
 ProtoMinor: (int) 1,
 Header: (http.Header) (len=5) {
  (string) (len=10) "User-Agent": ([]string) (len=1 cap=1) {
   (string) (len=11) "Mozilla/5.0"
  },
  (string) (len=13) "Cache-Control": ([]string) (len=1 cap=1) {
   (string) (len=8) "no-cache"
  },
  (string) (len=6) "Pragma": ([]string) (len=1 cap=1) {
   (string) (len=8) "no-cache"
  },
  (string) (len=6) "Accept": ([]string) (len=1 cap=1) {
   (string) (len=52) "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2"
  },
  (string) (len=10) "Connection": ([]string) (len=1 cap=1) {
   (string) (len=10) "keep-alive"
  }
 },
 Body: (*struct { http.eofReaderWithWriteTo; io.Closer })(0x7bf3d0)({
  eofReaderWithWriteTo: (http.eofReaderWithWriteTo) {
  },
  Closer: (ioutil.nopCloser) {
   Reader: (io.Reader) <nil>
  }
 }),
 ContentLength: (int64) 0,
 TransferEncoding: ([]string) <nil>,
 Close: (bool) false,
 Host: (string) (len=14) "127.0.0.1:8055",
 Form: (url.Values) <nil>,
 PostForm: (url.Values) <nil>,
 MultipartForm: (*multipart.Form)(<nil>),
 Trailer: (http.Header) <nil>,
 RemoteAddr: (string) (len=15) "127.0.0.1:39089",
 RequestURI: (string) (len=6) "/ci-cd",
 TLS: (*tls.ConnectionState)(<nil>),
 Cancel: (<-chan struct {}) <nil>,
 Response: (*http.Response)(<nil>),
 ctx: (*context.cancelCtx)(0xc4200f2040)(context.Background.WithValue(&http.contextKey{name:"http-server"}, &http.Server{Addr:"127.0.0.1:8055", Handler:(*main.LoggedMux)(0xc42000d260), ReadTimeout:0, WriteTimeout:0, TLSConfig:(*tls.Config)(0xc4200806e0), MaxHeaderBytes:0, TLSNextProto:map[string]func(*http.Server, *tls.Conn, http.Handler){"h2":(func(*http.Server, *tls.Conn, http.Handler))(0x4c5090), "h2-14":(func(*http.Server, *tls.Conn, http.Handler))(0x4c5090)}, ConnState:(func(net.Conn, http.ConnState))(nil), ErrorLog:(*log.Logger)(nil), disableKeepAlives:0, nextProtoOnce:sync.Once{m:sync.Mutex{state:0, sema:0x0}, done:0x1}, nextProtoErr:error(nil)}).WithValue(&http.contextKey{name:"local-addr"}, &net.TCPAddr{IP:net.IP{0x7f, 0x0, 0x0, 0x1}, Port:8055, Zone:""}).WithCancel.WithCancel)
})
)
```